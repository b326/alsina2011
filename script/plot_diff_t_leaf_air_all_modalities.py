"""
Plot the comparison of the difference between the leaf and air temperature for all modalities
"""
from subprocess import check_call

for season in ('fall', 'spring'):
    for rootstock in ('riparia', 'berlandieri'):
        check_call(['python', f'plot_diff_t_leaf_air.py', season, rootstock, 'batch'])
