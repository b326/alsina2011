"""
Format VPD between leaf and atmosphere measurements
"""
import json
from pathlib import Path

import pandas as pd
from graphextract.svg_extractor import extract_data

from alsina2011 import pth_clean

info = json.load(open(pth_clean / "info.json"))
meas_date = info['modalities']['when']

records = []
for season in ('spring', 'fall'):
    data = extract_data(f"../raw/fig3_{season}_vpd_leaf.svg",
                        x_formatter=lambda label: pd.Timestamp(f"{meas_date[season]}T{label}"),
                        y_formatter=float)

    for rootstock, points in data.items():
        for x, y in points:
            records.append(dict(
                when=season,
                rootstock=rootstock,
                date=x,
                vpd=y
            ))

df = pd.DataFrame(records).set_index(['when', 'rootstock', 'date'])

# write resulting dataframe in a csv file
df = df.sort_index()
with open(pth_clean / "vpd_leaf.csv", 'w', encoding="utf-8") as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write(f"# vpd: [kPa] vapor pressure deficit between leaf and atmosphere\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n")
