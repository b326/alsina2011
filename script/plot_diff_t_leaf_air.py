"""
Compare t_leaf - t_air with the graph t_leaf - t_air in Alsina
"""
import os
from sys import argv

import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import register_matplotlib_converters

from alsina2011 import pth_clean

try:
    when = argv[1]
    rootstock = argv[2]
except IndexError:
    when = 'spring'
    rootstock = 'riparia'

register_matplotlib_converters()

# read data
t_air = pd.read_csv(pth_clean / "t_air.csv", sep=';', comment='#', parse_dates=['date'],
                    index_col=['when', 'date'])
t_air = t_air.loc[when, 't_air']

t_leaf = pd.read_csv(pth_clean / "t_leaf.csv", sep=';', comment='#', parse_dates=['date'],
                     index_col=['when', 'rootstock', 'date'])
t_leaf = t_leaf.loc[(when, rootstock), 't_leaf']

diff_fig3 = pd.read_csv(pth_clean / "diff_t_air_leaf.csv", sep=';', comment='#', parse_dates=['date'],
                        index_col=['when', 'rootstock', 'date'])
diff_fig3 = diff_fig3.loc[(when, rootstock)]

# calculate difference between t_air and t_leaf
t_min = max(min(t_air.index), min(t_leaf.index))
t_max = min(max(t_air.index), max(t_leaf.index))

t_air = t_air[(t_air.index >= t_min) & (t_air.index <= t_max)]
t_leaf = t_leaf[(t_leaf.index >= t_min) & (t_leaf.index <= t_max)]

df = pd.DataFrame(index=pd.date_range(start=t_min, end=t_max, freq='10Min').floor('10Min'))

for date, val in t_air.items():
    df.loc[date.floor('10Min'), 't_air'] = val

for date, val in t_leaf.items():
    df.loc[date.floor('10Min'), 't_leaf'] = val

df = df.sort_index()

df = df.interpolate(method='time').resample('10Min').first()
df = df.dropna()
df['diff'] = df['t_leaf'] - df['t_air']

# plot comparison
if argv[-1] == 'batch':
    fig_size_x = 5
    fig_size_y = 3
else:
    fig_size_x = 8
    fig_size_y = 6

fig, axes = plt.subplots(1, 1, figsize=(fig_size_x, fig_size_y), squeeze=False)

ax = axes[0, 0]

ax.plot(df.index.time, df['diff'], 'o-', label="calculated with fig. 4 and fig. 3 data", markersize=3)
ax.plot(diff_fig3.index.time, diff_fig3, 'o-', label="data from fig. 3", markersize=3)

ax.legend(loc='upper right')
ax.set_ylabel("T leaf - T air  [°C]")

if argv[-1] == 'batch':
    ax.set_ylim(bottom=-5, top=15)
    fig.tight_layout()

    if not os.path.exists('res'):
        os.makedirs('res')

    plt.savefig(f"res/diff_t_leaf_air_{rootstock}_{when}.png")
else:
    ax.set_title(f"{when} / {rootstock}")
    fig.tight_layout()
    plt.show()
