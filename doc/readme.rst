Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://b326.gitlab.io/alsina2011/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/alsina2011/1.1.0/


.. image:: https://b326.gitlab.io/alsina2011/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/alsina2011


.. image:: https://b326.gitlab.io/alsina2011/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/alsina2011/


.. image:: https://badge.fury.io/py/alsina2011.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/alsina2011




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/alsina2011/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/alsina2011/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/alsina2011/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/alsina2011/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/alsina2011/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/alsina2011/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/alsina2011/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/alsina2011/commits/prod
.. #}

Data and formalisms from Alsina et al. (2011)
