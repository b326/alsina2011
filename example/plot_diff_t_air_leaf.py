"""
Debug leaf temperature
======================

Plot t_air - t_leaf to compare with fig3
"""
import matplotlib.pyplot as plt
import pandas as pd

from alsina2011 import pth_clean

# read data
df = pd.read_csv(pth_clean / "diff_t_air_leaf.csv", sep=';', comment='#', parse_dates=['date'],
                 index_col=['when', 'rootstock', 'date'])

# plot data
fig, axes = plt.subplots(1, 2, sharey='all', figsize=(11, 6), squeeze=False)

for i, season in enumerate(['spring', 'fall']):
    ax = axes[0, i]
    for rootstock in ('berlandieri', 'riparia'):
        sdf = df.loc[(season, rootstock)]
        time = sdf.index.hour + sdf.index.minute / 60
        ax.plot(time, sdf['diff_t_air_leaf'], 'o-', label=rootstock)

    ax.set_title(season)

ax = axes[0, 0]
ax.legend(loc='upper left')
ax.set_ylabel("t_air - t_leaf [°C]")

fig.tight_layout()
plt.show()
