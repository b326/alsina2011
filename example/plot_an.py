"""
Photosynthesis
==============

plot extracted An to compare to article
figure 3 B and H
"""
import matplotlib.pyplot as plt
import pandas as pd

from alsina2011 import pth_clean

# read data
df = pd.read_csv(pth_clean / "an.csv", sep=";", comment="#", parse_dates=['date'], index_col=['when', 'date'])

# plot data

fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(11, 6), squeeze=False)

for i, season in enumerate(['spring', 'fall']):
    ax = axes[0, i]

    for rootstock, sdf in df.loc[season].groupby(by='rootstock'):
        time = sdf.index.hour + sdf.index.minute / 60
        ax.plot(time, sdf['an'], 'o-', label=rootstock)

    ax.set_title(season)

ax = axes[0, 0]
ax.set_ylim(-5, 20)
ax.legend()
ax.set_ylabel("an [mumol CO2 m-2 s-1]")
for i in (0, 1):
    axes[0, i].set_xticks(range(4, 21, 4))
    axes[0, i].set_xlabel("")

fig.tight_layout()
plt.show()
