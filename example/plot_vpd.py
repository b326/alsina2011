"""
VPD
===

plot extracted vpd to compare to article
figure 4
"""
import matplotlib.pyplot as plt
import pandas as pd

from alsina2011 import pth_clean

# read data
df = pd.read_csv(pth_clean / "vpd.csv", sep=";", comment="#", parse_dates=['date'], index_col=['when', 'date'])

# plot data
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(11, 6), squeeze=False)

for i, when in enumerate(['spring', 'fall']):
    sdf = df.loc[when]
    t0 = pd.Timestamp(sdf.index[0].date())
    axes[0, i].plot([(t - t0).total_seconds() / 3600 for t in sdf.index], sdf['vpd'], 'o-')
    axes[0, i].set_title(when)

ax = axes[0, 0]
ax.set_ylim(0, 7)
ax.set_ylabel("vpd [kPa]")
ax.set_xlim(0, 24)
ax.set_xticks([0, 4, 8, 12, 16, 20, 24])

fig.tight_layout()
plt.show()
